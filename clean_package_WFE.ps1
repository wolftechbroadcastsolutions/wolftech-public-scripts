# replace_appsettings.ps1
param([Parameter(Mandatory = $True)][string]$folder)

function ProcessAppSettings($folder, $environment) {
    #Copy app.release.config to app.config and delete app.*.config
    $configFileName = "app.config"
    $configFile = "$folder\$configFileName"
    $envFileName = "app.$environment.config"
    $envFile = "$folder\$envFileName"

    if (-Not (Test-Path $configFile) -Or -Not (Test-Path $envFile)) {
        Write-Host "Cannot find file $configFile or $envFile"
        return;
    }

    Remove-Item $configFile
    Rename-Item -Path $envFile -NewName $configFileName
    Remove-Item "$folder\*"  -Include "app.*.config"
}

Write-Host "Cleaning up folder"
Write-Host "Deleting web.*.config"
Remove-Item "$folder\*"  -Include "web.*.config" -Exclude "web.config" 

ProcessAppSettings $folder "release"
