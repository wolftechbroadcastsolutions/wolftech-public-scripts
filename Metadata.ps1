﻿param([Parameter(Mandatory = $True)][string]$FileName,[int]$AppNumber = 1)

$repository = $env:APPVEYOR_REPO_NAME.Split("/")[1];
$jsonFileName = "$repository-$([guid]::NewGuid()).json"     

#append appnumber to repository and WT_METADATAFILE
$suffix = If ($AppNumber -gt 1) {"$($AppNumber)"} Else {""}
Set-Item "env:WT_METADATAFILE$($suffix)" $jsonFileName
$repository = "$($repository)$($suffix)"


$json = @{
    tag = $env:APPVEYOR_REPO_TAG 
    tagName = $env:APPVEYOR_REPO_TAG_NAME 
    repository = $repository
    branch = $env:APPVEYOR_REPO_BRANCH
    commit =  $env:APPVEYOR_REPO_COMMIT
    buildVersion = $env:APPVEYOR_BUILD_VERSION
    buildNumber = $env:APPVEYOR_BUILD_NUMBER -As [int]
    accountName = $env:APPVEYOR_ACCOUNT_NAME
    fileName = $FileName
    appNumber = $AppNumber
} | ConvertTo-Json

#Mysterious way to create UTF-8 (not UTF-8 with BOM)
$utf8 = New-Object System.Text.UTF8Encoding $false
Set-Content -Value $utf8.GetBytes($json) -Encoding Byte -Path $jsonFileName