﻿function InstallAndRun-Redis {
    mkdir C:\\Software\\Redis
    wget https://github.com/redis-windows/redis-windows/releases/download/7.2.4/Redis-7.2.4-Windows-x64-msys2-with-Service.zip -OutFile c:\\Software\\Redis.zip
    Expand-Archive C:\\Software\\Redis.zip -DestinationPath C:\\Software\\Redis
    sc.exe Create Redis binpath=C:\\Software\\Redis\\Redis-7.2.4-Windows-x64-msys2-with-Service\\RedisService.exe start=auto
    net start Redis
    tnc localhost -Port 6379 # Test Redis connection
}

 function Build-Filter {
    param($FromChar, $ToChar)
    $filter = ""
    For ($letter = $FromChar; $letter -le $ToChar; $letter++) {
        $partial = "FullyQualifiedName~Wolftech.IntegrationTests.$([char]$letter)"
        $filter = "$filter|$partial"
    }
    return $filter.Substring(1)
}
          
function DownloadExtract-Artifact {
    param ($BuildVersion,$WorkingFolder,$Token,$ArtifactName,$Project)
    $f = New-Item -Path $WorkingFolder -ItemType Directory -Force
    $project = Invoke-RestMethod -Method Get -Uri "https://ci.appveyor.com/api/projects/wolftech/$Project/build/$BuildVersion" -Headers @{"Authorization" = "Bearer $Token"; "Content-type" = "application/json"}
    $artifactUrl = "https://ci.appveyor.com/api/buildjobs/$($project.build.jobs[0].jobId)/artifacts/$ArtifactName"
    Write-Host "Downloading $artifactUrl to $WorkingFolder\$ArtifactName"
    Write-Host "Token is $Token"
    Invoke-WebRequest -Uri $artifactUrl  -OutFile "$WorkingFolder\$ArtifactName" -Headers @{ "Authorization" = "Bearer $Token" }
    Expand-Archive "$WorkingFolder\$ArtifactName" -DestinationPath $WorkingFolder
}

 function Download-Artifact {
    param ($BuildVersion,$WorkingFolder,$Token,$ArtifactName,$Project)
    $f = New-Item -Path $WorkingFolder -ItemType Directory -Force
    $project = Invoke-RestMethod -Method Get -Uri "https://ci.appveyor.com/api/projects/wolftech/$Project/build/$BuildVersion" -Headers @{"Authorization" = "Bearer $Token"; "Content-type" = "application/json"}
    $artifactUrl = "https://ci.appveyor.com/api/buildjobs/$($project.build.jobs[0].jobId)/artifacts/$ArtifactName"
    Write-Host "Downloading $artifactUrl to $WorkingFolder\$ArtifactName"
    Write-Host "Token is $Token"
    Invoke-WebRequest -Uri $artifactUrl -OutFile "$WorkingFolder\$ArtifactName" -Headers @{ "Authorization" = "Bearer $Token" }
}

function DownloadArtifactRun-Tests {
     param ($Token,$ArtifactName,$Project, $TestFile, $FromChar=-1, $ToChar=-1)
    
    #Define variables
    $guid = New-Guid
    $workingFolder = "$((Get-Item .).FullName)\$guid"
    Write-Host "Working folder is $workingFolder"

    #Download artifact
    DownloadExtract-Artifact -BuildVersion $env:APPVEYOR_BUILD_VERSION -WorkingFolder $workingFolder -Token $env:APPVEYOR_API_TOKEN -Project "wolftech-news-core" -ArtifactName $ArtifactName

    #Run tests
    if($FromChar -eq -1) {
        dotnet test "./$guid/$TestFile" --test-adapter-path:$workingFolder --logger:Appveyor
       
    }
    else {
        $filter = Build-Filter -FromChar $FromChar -ToChar $ToChar
        Write-Host "Filter is $filter"
        dotnet test "./$guid/$TestFile" --test-adapter-path:$workingFolder --logger:Appveyor --filter $filter
    }
    
    if ($LASTEXITCODE -ne 0) {
        throw "Test run for $($Project) failed"
    }

}