# replace_appsettings.ps1
param([Parameter(Mandatory = $True)][string]$folder)

function ProcessJsonConfig($folder, $environment) {
    #Copy appsettings.release.json to appsettings.json and delete appsettings.*.json
    $configFileName = "appsettings.json"
    $configFile = "$folder\$configFileName"
    $envFileName = "appsettings.$environment.json"
    $envFile = "$folder\$envFileName"

    if (-Not (Test-Path $configFile) -Or -Not (Test-Path $envFile)) {
        Write-Host "Cannot find file $configFile or $envFile"
        return;
    }

    Remove-Item $configFile
    Rename-Item -Path $envFile -NewName $configFileName
    Remove-Item "$folder\*"  -Include "appsettings.*.json"

}

function ProcessAppSettings($folder, $environment) {
    #Copy app.release.config to app.config and delete app.*.config
    $configFileName = "app.config"
    $configFile = "$folder\$configFileName"
    $envFileName = "app.$environment.config"
    $envFile = "$folder\$envFileName"

    if (-Not (Test-Path $configFile) -Or -Not (Test-Path $envFile)) {
        Write-Host "Cannot find file $configFile or $envFile"
        return;
    }

    Remove-Item $configFile
    Rename-Item -Path $envFile -NewName $configFileName
    Remove-Item "$folder\*"  -Include "app.*.config"
}

function IgnoreSettings($configPath) {
    #Feed services would like to keep this setting
    return @("SubscriptionQueuename", "ServiceName")
}

function ResetAppsettings($configPath) {
    if (-Not (Test-Path $configPath)) {
        Write-Host "Cannot find file $configPath"
        return;
    }

    $xml = [xml](Get-Content $configPath)
    $ignore = IgnoreSettings $configPath

    Write-Output "Resetting appsettings for file $configPath"
    ForEach ($add in $xml.configuration.appSettings.add) {
        if ($ignore.Contains($add.key)) {
            Write-Output "Ignoring setting $($add.key)"
            continue
        }
        
        $matchingEnvVar = [Environment]::GetEnvironmentVariable($add.key)
        if ($matchingEnvVar) {
            $add.value = $matchingEnvVar
        }
        else {
            $add.value = ""
        }
    }

    Write-Output "Resetting entity framework connection for file $configPath"
    try {
        ForEach ($add in $xml.configuration.entityFramework.defaultConnectionFactory.parameters.parameter) {
            $add.value = "#{ConnectionString}"
        }
    }
    catch {
        Write-Host "Could not replace entity framework connectionstring"
    }
    $xml.Save($configPath)

}

Write-Host "Cleaning up folder"
Write-Host "Deleting web.*.config"
Remove-Item "$folder\*"  -Include "web.*.config" -Exclude "web.config" 

ProcessAppSettings $folder "release"
ResetAppsettings("$folder\web.config")
ResetAppsettings("$folder\app.config")
ProcessJsonConfig $folder "release"