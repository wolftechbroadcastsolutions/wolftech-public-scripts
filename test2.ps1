function TestIt(){

    Write-Host "Writing out env variables"
    Write-Host $env:appveyor_build_version
    Write-Host $env:GitUsername
    Write-Host $env:APPVEYOR_REPO_NAME
    Write-Host $env:APPVEYOR_REPO_COMMIT
    Write-Host "Done writing"

    git remote add bitbucket https://$($env:GitUsername):$($env:GitPassword)@bitbucket.org/$($env:APPVEYOR_REPO_NAME).git
    git tag $($env:appveyor_build_version) $($env:APPVEYOR_REPO_COMMIT)
    git push bitbucket --tags --quiet
}