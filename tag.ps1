
function Add-Tag(){
Write-Host "Add Tag function on $($env:APPVEYOR_REPO_BRANCH)"
    $global:ErrorActionPreference = "Continue"
    try {       
            Write-Host "Adding Tag on $($env:APPVEYOR_REPO_NAME)"
        if(("$($env:APPVEYOR_REPO_BRANCH)" -like "*master*") -or ("$($env:APPVEYOR_REPO_BRANCH)" -like "release*")){
            if(!([string]::IsNullOrEmpty($env:git_access_token))){
             git config user.email $($env:git_email)
             git remote add bitbucket-test https://x-token-auth:$($env:git_access_token)@bitbucket.org/$($env:APPVEYOR_REPO_NAME).git
             git tag $($env:appveyor_build_version) $($env:APPVEYOR_REPO_COMMIT)
             git push bitbucket-test --tags --quiet
             if ($LastExitCode -ne 0){
                 $ErrorActionPreference = "Continue"
                 Throw
             }
            }
        }
    }
    catch { 
        $global:LASTEXITCODE = 0

    }
}

Add-Tag